#!/usr/bin/env python3
from collections import Counter
from jinja2 import Environment, FileSystemLoader
from time import strptime, strftime
import bs4 as bs
import calendar
import argparse
import os.path
import json
import copy
import re


def init_logfile(logfile):
    if os.path.isfile(logfile):
        return
    else:
        with open(logfile, 'w') as fp:
            json.dump([], fp)


def log_jump(logfile):
    def get_field(name, common):
        if common:
            spaces = ' ' * (29 - (2 + len(common) + 15))
            print('{:15s}{}({}): '.format(name, spaces, common), end='')
        else:
            print('{:29s}: '.format(name), end='')
        field = input()

        if common and not field:
            return common
        return field

    def get_common(field, data):
        if data:
            return sorted(Counter(j[field] for j in data).items(), 
                          key=lambda x: x[1])[-1][0]
        else:
            return None
    
    with open(logfile, 'r') as fp:
        data = json.load(fp)
    jump = {}

    # Get most common values
    common_loc = get_common('location', data)
    common_plane = get_common('plane', data)
    common_altitude = get_common('altitude', data)
    common_canopy = get_common('canopy', data)
    common_type = get_common('jump_type', data)
            
    jump['number'] = get_field('Number', None)
    if len(jump['number']) == 0 or jump['number'] == '':
        return False

    jump['date'] = get_field('Date (ddmmyy)', common=None)
    jump['location'] = get_field('Location', common_loc)
    jump['plane'] = get_field('Plane', common_plane)
    jump['altitude'] = get_field('Altitude', common_altitude)
    jump['jump_type'] = get_field('Jump Type', common=common_type)
    jump['canopy'] = get_field('Canopy', common_canopy)
    jump['notes'] = get_field('Notes', common=None)
    
    # Add youtube link to notes if any
    yt = get_field('Youtube', common=None)
    if yt:
        jump['notes'] += '\n' + yt

    # Allow entering new lines with \n
    jump['notes'] = jump['notes'].replace('\\n', '\n')

    # jump already exists
    if jump in data:
        print('Jump already exists in logbook -> not added\n')
        return True

    data.append(jump)
    print('\n')
    with open(logfile, 'w') as fp:
        json.dump(sorted(data, key=lambda x: int(x['number'])), 
                  fp,
                  sort_keys=True,
                  ensure_ascii=False,
                  indent=4)

    return True


def loop_log(logfile):
    while True:
        if not log_jump(logfile):
            break


def make_stats(logfile, display=True):
    def get_numbers(field, data):
        return sorted(Counter(j[field] for j in data).items(), 
                key=lambda x: x[0], reverse=True)

    with open(logfile, 'r') as fp:
        data = json.load(fp)

    fields = ['altitude', 'canopy', 'jump_type', 'location', 'plane']

    res = {}
    for field in fields:
        numbers = get_numbers(field, data)
        res[field] = numbers
        if display:
            print(field.title())
            print('-' * 24)
            for value, count in numbers:
                print('  {:10s} {} jumps'.format(value, count))
            print('')

    return res


def render_template(filename, out_fname, data):
    env = Environment(loader=FileSystemLoader('templates'))
    template = env.get_template(filename)
    rendered = template.render(**data)

    out = open(out_fname, 'w')
    out.write(rendered)


def get_videos(data):
    jumps_video = copy.deepcopy(data)
    jumps_video = [j for j in jumps_video if 'youtube' in j['notes']]
    for j in jumps_video:
        if 'youtube' in j['notes']:
            reg = r"http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\\" \
                  r".be\/)([\w\-\_]*)(&(amp;)?​[\w\?​=]*)?"
            sub = r'<div class="video_sup">' \
                  r'<div class="video_container">' \
                  r'<iframe class="video"' \
                  r'allowfullscreen="allowfullscreen"' \
                  r'src="https://www.youtube.com/embed/\1"></iframe>' \
                  r'</div>' \
                  r'</div>'
            j['notes'] = re.sub(reg, sub, j['notes'])
    return jumps_video


def make_calendar(data):
    dates = {}
    for jump in data:
        if jump['date'] not in dates:
            dates[jump['date']] = 1
        else:
            dates[jump['date']] += 1

    days = [(word[:3], word[0]) for word in calendar.day_name]
    different_years = sorted(set([int(y.split('-')[0]) for y in dates]),
                             reverse=True)

    number_jumps_per_year = { year: 0 for year in different_years }
    number_jumps_per_month = {}

    for date, number in dates.items():
        number_jumps_per_year[int(date.split('-')[0])] += number

        if date[:-3] not in number_jumps_per_month.keys():
            number_jumps_per_month[date[:-3]] = 0
        number_jumps_per_month[date[:-3]] += number
    
    result = []
    # Loop through every year
    for year in different_years:
        c = calendar.HTMLCalendar(calendar.MONDAY)
        html = c.formatyear(year)
        html = bs.BeautifulSoup(html, 'lxml') # html of year
        
        # Replace day names by their first letter
        for day, letter in days:
            texts = html.find_all(text=day)
            for t in texts:
                new_text = str(t).replace(day, letter)
                t.replace_with(new_text)

        # Add total number of jumps under year's number
        # fuck, that's hard to do with this library
        br = html.new_tag('br')
        year_td = html.find(text=year).parent
        
        year_td.string = ''
        year_td.append(bs.NavigableString(str(year)))
        year_td.append(br)

        nb_j = str(number_jumps_per_year[year]) + ' jumps'
        year_td.append(bs.NavigableString(nb_j))
        
        # Add color for day containing a jump
        for date in dates.keys():
            y = int(date.split('-')[0])
            m = int(date.split('-')[1])
            d = int(date.split('-')[2])

            if y != year:
                continue
            
            html_month = html.find(text=calendar.month_name[m])\
                             .parent.parent.parent
            jump_day = html_month.find(text=d).parent
        
            # Add number of jumps
            month_tag = html.find('th', {'class': 'month'}, text=calendar.month_name[m])
            if month_tag:
                br = html.new_tag('br')
                month_name = month_tag.text
                month_tag.string = ''
                month_tag.append(bs.NavigableString(month_name))
                month_tag.append(br)
                nb_j = str(number_jumps_per_month[date[:-3]]) + ' jumps'
                month_tag.append(bs.NavigableString(nb_j))
            
            if dates[date] >= 4:
                color = 'red'
            elif dates[date] >= 3:
                color = 'orange'
            elif dates[date] == 2:
                color = 'yellow'
            else: # one jump
                color = 'green'
            jump_day['class'] += ['color-' + color]

        result.append(html)

    return result


def get_licenses(logfile):
    with open(logfile, 'r') as fp:
        try:
            licenses = json.load(fp)
        except:
            licenses = []

    for l in licenses:
        date = strptime(l['date'], '%d%m%y')
        l['date'] = strftime('%Y-%m-%d', date)

    return sorted(licenses, key=lambda x: x['date'])


def write_licenses(logfile):
    with open(logfile, 'r') as fp:
        try:
            licenses = json.load(fp)
        except:
            licenses = []

    while True:
        l = {}
        l['name'] = input('License: ') 
        if l['name'] == '':
            break
        l['date'] = input('Date (ddmmyy): ')
        l['number'] = input('Number: ')
        l['loc'] = input('Location: ')
        licenses.append(l)

    with open(logfile, 'w') as fp:
        json.dump(sorted(licenses, key=lambda x: x['name']), 
                  fp,
                  indent=4)


def make_html(logfile):
    with open(logfile, 'r') as fp:
        data = json.load(fp)
    
    with open('licenses.json', 'r') as fp:
        licenses = json.load(fp)

    for jump in data:
        date = strptime(jump['date'], '%d%m%y')
        jump['date'] = strftime('%Y-%m-%d', date)

    data = data[::-1]  # last jumps first

    jumps_video = get_videos(data)
    stats = make_stats(logfile, display=False)
    years_calendar = make_calendar(data)
    licenses = get_licenses('licenses.json')

    render_template('index.html', 
                    'index.html', 
                    {'jumps': data})
    render_template('stats.html', 
                    'stats.html', 
                    {'stats': stats})
    render_template('video.html', 
                    'video.html', 
                    {'jumps': jumps_video})
    render_template('calendar.html', 
                    'calendar.html', 
                    {'years': years_calendar})
    render_template('licenses.html', 
                    'licenses.html', 
                    {'licenses': licenses})
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', type=str, default='jumps.json')
    parser.add_argument('--html', action='store_true', 
                        default=False)
    parser.add_argument('--license', action='store_true', 
                        default=False)
    args = parser.parse_args()

    if args.html:
        make_html(args.file)
        exit()

    if args.license:
        write_licenses('licenses.json')
        exit()

    init_logfile(args.file)
    loop_log(args.file)
